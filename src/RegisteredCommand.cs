﻿using uMod.Common;

namespace uMod.Mock
{
    // Registered commands
    public class RegisteredCommand
    {
        /// <summary>
        /// The plugin that handles the command
        /// </summary>
        public readonly IPlugin Source;

        /// <summary>
        /// The name of the command
        /// </summary>
        public readonly string Command;

        /// <summary>
        /// The callback
        /// </summary>
        public readonly CommandCallback Callback;

        /// <summary>
        /// Initializes a new instance of the RegisteredCommand class
        /// </summary>
        /// <param name="source"></param>
        /// <param name="command"></param>
        /// <param name="callback"></param>
        public RegisteredCommand(IPlugin source, string command, CommandCallback callback)
        {
            Source = source;
            Command = command;
            Callback = callback;
        }
    }
}
