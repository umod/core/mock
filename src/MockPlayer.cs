﻿extern alias References;
using System;
using System.Globalization;
using References::Newtonsoft.Json;
using References::ProtoBuf;
using uMod.Common;

namespace uMod.Mock
{
    public class MockPlayer : IPlayer, IEquatable<IPlayer>
    {
        #region Initialization

        private readonly ILibrary _permissions;
        private readonly ILibrary _lang;

        private readonly GamePlayer _player;

        public MockPlayer(string playerId, string playerName)
        {
            _permissions = BaseTest.CurrentTest.CurrentScope.Module.Libraries.Get("Permission");
            _lang = BaseTest.CurrentTest.CurrentScope.Module.Libraries.Get("Lang");

            Name = playerName;
            Id = playerId;
        }

        public MockPlayer(GamePlayerIdentity player) : this(player.ID.ToString(), player.GamePlayer.displayName)
        {
            _player = player.GamePlayer;
            player.GamePlayer.IPlayer = this;
        }

        public MockPlayer(GamePlayer player) : this(player.Identity.ID.ToString(), player.displayName)
        {
            _player = player;
            player.IPlayer = this;
        }

        public void Reconnect(object player)
        {
            Object = player;
        }

        #endregion Initialization

        #region Objects

        [JsonIgnore, ProtoIgnore]
        public object Object { get => _player; set { } }

        [JsonIgnore, ProtoIgnore]
        public CommandType LastCommand { get; set; }

        #endregion Objects

        #region Information

        public string Name { get; set; }

        public string Id { get; }

        public CultureInfo Language => CultureInfo.GetCultureInfo(_lang.Invoke<string>("GetLanguage", Id));

        public string Address { get; set; }

        public int Ping => 0;

        public bool IsAdmin { get; set; }

        public bool IsModerator { get; set; }

        public bool IsBanned { get; set; }

        public bool IsConnected { get; set; }

        public bool IsAlive { get; set; }

        public bool IsDead { get; set; }

        public bool IsSleeping { get; set; }

        public bool IsServer => false;

        public bool IsReturningPlayer { get; set; }

        public bool HasSpawnedBefore { get; set; }

        #endregion Information

        #region Administration

        public void Ban(string reason, TimeSpan duration = default) => IsBanned = true;

        public TimeSpan BanTimeRemaining => IsBanned ? TimeSpan.MaxValue : TimeSpan.Zero;

        public void Kick(string reason) => _player.Kick(reason);

        public void Unban() => IsBanned = false;

        #endregion Administration

        #region Character

        public void Heal(float amount) => _player.Heal(amount);

        public float Health
        {
            get => _player.health;
            set => _player.health = value;
        }

        public float MaxHealth
        {
            get => 100;
            set => throw new NotImplementedException();
        }

        public void Hurt(float amount) => _player.Hurt(amount);

        public void Kill() => _player.Die();

        public void Rename(string playerName) => _player.displayName = playerName;

        public void Reset()
        {
        }

        public void Respawn()
        {
        }

        public void Respawn(Position pos)
        {
        }

        #endregion Character

        #region Positional

        public void Position(out float x, out float y, out float z)
        {
            Position pos = _player.position;
            x = pos.X;
            y = pos.Y;
            z = pos.Z;
        }

        public Position Position() => _player.position;

        public void Teleport(float x, float y, float z)
        {
            _player.position.X = x;
            _player.position.Y = y;
            _player.position.Z = z;
        }

        public void Teleport(Position pos) => Teleport(pos.X, pos.Y, pos.Z);

        #endregion Positional

        #region Chat and Commands

        public void Message(string message, string prefix, params object[] args)
        {
        }

        public void Message(string message) => Message(message, null);

        public void Reply(string message, string prefix, params object[] args)
        {
        }

        public void Reply(string message) => Reply(message, null);

        public void Command(string command, params object[] args)
        {
        }

        #endregion Chat and Commands

        #region Permissions

        /// <summary>
        /// Gets if the player has the specified permission
        /// </summary>
        /// <param name="perm"></param>
        /// <returns></returns>
        public bool Can(string perm) => _permissions.Invoke<bool>("UserHasPermission", Id, perm);

        /// <summary>
        /// Gets if the player has the specified permission
        /// </summary>
        /// <param name="perm"></param>
        /// <returns></returns>
        public bool HasPermission(string perm) => _permissions.Invoke<bool>("UserHasPermission", Id, perm);

        /// <summary>
        /// Grants the specified permission on this player
        /// </summary>
        /// <param name="perm"></param>
        public bool GrantPermission(string perm) => _permissions.Invoke<bool>("GrantUserPermission", Id, perm, null);

        /// <summary>
        /// Strips the specified permission from this player
        /// </summary>
        /// <param name="perm"></param>
        public bool RevokePermission(string perm) => _permissions.Invoke<bool>("RevokeUserPermission", Id, perm);

        /// <summary>
        /// Gets if the player belongs to the specified group
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public bool BelongsToGroup(string group) => _permissions.Invoke<bool>("UserHasGroup", Id, group);

        /// <summary>
        /// Adds the player to the specified group
        /// </summary>
        /// <param name="group"></param>
        public bool AddToGroup(string group) => _permissions.Invoke<bool>("AddUserGroup", Id, group);

        /// <summary>
        /// Removes the player from the specified group
        /// </summary>
        /// <param name="group"></param>
        public bool RemoveFromGroup(string group) => _permissions.Invoke<bool>("RemoveUserGroup", Id, group);

        #endregion Permissions

        #region Operator Overloads

        /// <summary>
        /// Gets if player's unique ID is equal to another player's unique ID
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(IPlayer other) => Id == other?.Id;

        /// <summary>
        /// Gets if player's object is equal to another player's object
        /// </summary>
        /// <param name="playerObj"></param>
        /// <returns></returns>
        public override bool Equals(object playerObj) => playerObj is IPlayer player && Id == player.Id;

        /// <summary>
        /// Gets the hash code of the player's unique ID
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode() => Id.GetHashCode();

        /// <summary>
        /// Returns a human readable string representation of this IPlayer
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"MockPlayer[{Id}, {Name}]";

        #endregion Operator Overloads
    }
}
