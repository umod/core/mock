﻿using uMod.Common;
using uMod.Common.Command;

namespace uMod.Mock
{
    public class MockProvider : IUniversalProvider
    {
        public static MockCommandSystem CommandSystem;
        public static MockServer Server;

        public string GameName => "Testerino";

        public uint ClientAppId => uint.MinValue;

        public uint ServerAppId => uint.MaxValue;

        public IGameTypes Types { get; private set; }

        public ICommandSystem CreateCommandSystemProvider(ICommandHandler commandHandler)
        {
            return CommandSystem = new MockCommandSystem(commandHandler);
        }

        public IPlayerManager CreatePlayerManager(IApplication application, ILogger logger)
        {
            return null;
        }

        public IServer CreateServer()
        {
            Server = new MockServer();
            Types = new MockGameTypes(Server);
            return Server;
        }

        public string FormatText(string text)
        {
            return text;
        }
    }
}
