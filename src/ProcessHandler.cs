﻿extern alias References;

using References::Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;

namespace uMod.Mock
{
    internal class ProcessHandler
    {
        private static string GetAgentArtifactUrl(bool developBranch)
        {
            string filePath = Path.Combine(Environment.CurrentDirectory, "uMod.Manifest.json");

            using (WebClient client = new WebClient())
            {
                client.DownloadFile("https://assets.umod.org/uMod.Manifest.json", filePath);

                if (!File.Exists(filePath))
                {
                    throw new Exception("Unable to obtain manifest:");
                }
            }

            JObject manifest = JObject.Parse(File.ReadAllText(filePath));

            if (!(manifest["Packages"] is JArray array))
            {
                throw new Exception("Unable to obtain agent install package from manifest");
            }

            JToken agentPackage = null;

            foreach (JToken package in array)
            {
                if (package["Name"].ToString() == "core/agent")
                {
                    agentPackage = package;
                    break;
                }
            }

            if (agentPackage == null)
            {
                throw new Exception("Unable to obtain agent package from manifest");
            }

            JToken installResource = null;
            if (developBranch)
            {
                foreach (JToken resource in agentPackage["Resources"])
                {
                    if (resource["Version"].ToString() == "develop" && resource["Type"].ToString() == "4")
                    {
                        installResource = resource;
                        break;
                    }
                }
            }

            if (installResource == null)
            {
                foreach (JToken resource in agentPackage["Resources"])
                {
                    if (resource["Type"].ToString() == "4")
                    {
                        installResource = resource;
                        break;
                    }
                }
            }

            if (installResource == null)
            {
                throw new Exception("Unable to obtain agent install artifact from manifest");
            }

            string artifactUrl = string.Empty;
            foreach (JToken artifact in installResource["Artifacts"])
            {
                if ((Environment.OSVersion.Platform == PlatformID.Unix && artifact["Platform"].ToString() == "linux") ||
                    (Environment.OSVersion.Platform == PlatformID.Win32NT && artifact["Platform"].ToString() == "windows"))
                {
                    artifactUrl = artifact["Url"].ToString();
                }
            }

            if (string.IsNullOrEmpty(artifactUrl))
            {
                throw new Exception("Unable to obtain agent artifact URL");
            }

            return artifactUrl;
        }

        public static void DownloadAgent(string targetDirectory, bool developBranch)
        {
            string artifactUrl = GetAgentArtifactUrl(developBranch);
            string fileName = Path.GetFileName(artifactUrl);
            string fullPath = Path.Combine(Environment.CurrentDirectory, fileName);

            using (WebClient client = new WebClient())
            {
                client.DownloadFile(artifactUrl, fullPath);
            }

            #if NET48
            ZipFile.ExtractToDirectory(fullPath, targetDirectory);
            #else
            ZipFile.ExtractToDirectory(fullPath, targetDirectory, true);
            #endif
        }

        /// <summary>
        /// Execute arbitrary system command
        /// </summary>
        /// <param name="cmd"></param>
        internal static void Exec(string cmd)
        {
            using(Process process = CreateExecProcess(cmd))
            {
                process.Start();
                process.WaitForExit();
            }
        }

        /// <summary>
        /// Create unix bash process
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        private static Process CreateExecProcess(string cmd)
        {
            string escapedArgs = cmd.Replace("\"", "\\\"");

            return new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{escapedArgs}\""
                }
            };
        }
    }
}
