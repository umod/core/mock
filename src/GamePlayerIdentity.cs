﻿namespace uMod.Mock
{
    public class GamePlayerIdentity
    {
        public ulong ID = 76561197960265729;

        public GamePlayer GamePlayer;

        public GamePlayerIdentity()
        {
            GamePlayer = new GamePlayer(this);
        }
    }
}
