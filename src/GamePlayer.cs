﻿using uMod.Common;

namespace uMod.Mock
{
    public sealed class GamePlayer
    {
        public float health = 100;
        public string displayName = "Mocky McMockerson";
        public Position position = new Position(1, 2, 3);
        public IPlayer IPlayer;

        public GamePlayerIdentity Identity;

        public GamePlayer(GamePlayerIdentity identity)
        {
            Identity = identity;
        }

        public void Heal(float amt)
        {
            health += amt;
        }

        public void Hurt(float amt)
        {
            health -= amt;
        }

        public void Kick(string reason)
        {
            IPlayer?.Kick(reason);
        }

        public void Die()
        {
            health = 0;
        }
    }
}
