using System;
using System.Globalization;
using uMod.Common;

namespace uMod.Mock
{
    public class MockServerPlayer : IPlayer
    {
        public void Reconnect(object player)
        {
            Object = player;
        }

        #region Objects

        public object Object { get; set; }

        public CommandType LastCommand { get => CommandType.Console; set { } }

        #endregion Objects

        #region Information

        public string Name { get => "Server Console"; set { } }

        public string Id => "server_console";

        public string Address => "127.0.0.1";

        public int Ping => 0;

        public CultureInfo Language => CultureInfo.InstalledUICulture;

        public bool IsAdmin => true;

        public bool IsModerator => true;

        public bool IsBanned => false;

        public bool IsConnected => true;

        public bool IsAlive => true;

        public bool IsDead => false;

        public bool IsSleeping => false;

        public bool IsServer => true;

        public bool IsReturningPlayer { get => true; set { } }

        public bool HasSpawnedBefore { get => true; set { } }

        #endregion Information

        #region Administration

        public void Ban(string reason, TimeSpan duration = default)
        {
        }

        public TimeSpan BanTimeRemaining => TimeSpan.Zero;

        public void Kick(string reason)
        {
        }

        public void Unban()
        {
        }

        #endregion Administration

        #region Character

        public void Heal(float amount)
        {
        }

        public float Health { get; set; }

        public float MaxHealth { get; set; }

        public void Hurt(float amount)
        {
        }

        public void Kill()
        {
        }

        public void Rename(string playerName)
        {
        }

        public void Reset()
        {
        }

        public void Respawn()
        {
        }

        public void Respawn(Position pos)
        {
        }

        #endregion Character

        #region Positional

        public void Position(out float x, out float y, out float z)
        {
            x = 0;
            y = 0;
            z = 0;
        }

        public Position Position() => new Position(0, 0, 0);

        public void Teleport(float x, float y, float z)
        {
        }

        public void Teleport(Position pos)
        {
        }

        #endregion Positional

        #region Permissions

        public bool Can(string perm) => false;

        public bool HasPermission(string perm) => false;

        public bool GrantPermission(string perm)
        {
            return true;
        }

        public bool RevokePermission(string perm)
        {
            return false;
        }

        public bool BelongsToGroup(string group) => false;

        public bool AddToGroup(string group)
        {
            return true;
        }

        public bool RemoveFromGroup(string group)
        {
            return false;
        }

        #endregion Permissions

        #region Chat and Commands

        public void Message(string message, string prefix, params object[] args)
        {
            string formatted = prefix != null ? $"{prefix} {message}" : message;
            BaseTest.CurrentTest.CurrentScope.Module.LogInfo(formatted);
        }

        public void Message(string message) => Message(message, null);

        public void Reply(string message, string prefix, params object[] args)
        {
            Message(message, prefix, args);
        }

        public void Reply(string message)
        {
            Message(message, null);
        }

        public void Command(string command, params object[] args)
        {
            MockProvider.CommandSystem.Handler.HandleConsoleMessage(MockServer.ServerPlayer, $"{command} {string.Join(" ", args)}");
        }

        #endregion Chat and Commands
    }
}
