﻿using System;
using System.Reflection;
using uMod.Common;

namespace uMod.Mock
{
    public class MockGameTypes : IGameTypes
    {
        public Type Player { get; } = typeof(GamePlayer);

        private readonly IServer _server;
        private bool _playerFieldFound;
        private FieldInfo _playerField;

        public MockGameTypes(IServer server)
        {
            _server = server;
        }

        public IPlayer GetPlayer(object gamePlayer)
        {
            if (!_playerFieldFound)
            {
                _playerField = Player.GetField("IPlayer", BindingFlags.Instance | BindingFlags.Public);
                _playerFieldFound = true;
            }

            if (_playerField != null)
            {
                return _playerField.GetValue(gamePlayer) as IPlayer;
            }

            return _server.PlayerManager.FindPlayerByObj(gamePlayer);
        }
    }
}
