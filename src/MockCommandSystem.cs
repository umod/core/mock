﻿using System;
using System.Collections.Generic;
using uMod.Common;
using uMod.Common.Command;

namespace uMod.Mock
{
    public class MockCommandSystem : ICommandSystem
    {
        private readonly IDictionary<string, RegisteredCommand> registeredCommands = new Dictionary<string, RegisteredCommand>(StringComparer.OrdinalIgnoreCase);

        public readonly ICommandHandler Handler;

        internal MockCommandSystem(ICommandHandler commandHandler)
        {
            Handler = commandHandler;
            commandHandler.CommandFilter = registeredCommands.ContainsKey;
            commandHandler.Callback = CommandCallback;
        }

        public void RegisterCommand(string command, IPlugin plugin, CommandCallback callback)
        {
            registeredCommands.Add(command, new RegisteredCommand(plugin, command, callback));
        }

        public void UnregisterCommand(string command, IPlugin plugin)
        {
            registeredCommands.Remove(command);
        }

        public CommandState CommandCallback(IPlayer caller, string command, string fullCommand, object[] context = null, ICommandInfo commandInfo = null)
        {
            if (!registeredCommands.TryGetValue(command, out RegisteredCommand registeredCommand))
            {
                return CommandState.Unrecognized;
            }

            return registeredCommand.Callback(caller, command, fullCommand, context);
        }

        public CommandState HandleChatMessage(IPlayer player, string message) => Handler.HandleChatMessage(player, message);
    }
}
